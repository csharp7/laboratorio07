﻿using System;

namespace Laboratorio7
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lista = { "Julio", "Lucia", "Daniel", "Joao" };
            Console.WriteLine("Array sem ordenação");
            for (int i = 0; i < lista.Length; i++)
            {
                Console.WriteLine(lista[i] + " ");
            }

            Array.Sort(lista);
            Console.WriteLine("Array com ordenação");
            for (int i = 0; i < lista.Length; i++)
            {
                Console.WriteLine(lista[i] + " ");
            }

            Console.WriteLine();
            Pessoa[] lista2 = {
                new Pessoa("Jose", 25),
                new Pessoa("Ana", 28),
                new Pessoa("Paulo", 20)
            };
            Array.Sort(lista2, Pessoa.CompareByNome);
            Console.WriteLine("Array com ordenação por nome");
            for (int i = 0; i < lista2.Length; i++)
            {
                Console.WriteLine(lista2[i].Nome + " " + lista2[i].Idade);
            }

             Array.Sort(lista2, Pessoa.ComparebyIdade);
            Console.WriteLine("Array com ordenação por idade");
            for (int i = 0; i < lista2.Length; i++)
            {
                Console.WriteLine(lista2[i].Idade+ " " + lista2[i].Nome);
            }

        }
    }
}
