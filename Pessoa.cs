using System;

namespace Laboratorio7
{
    public class Pessoa
    {
        private string nome;
        private int idade;

        public Pessoa(string n, int i)
        {
            nome = n;
            idade = i;
        }

        public string Nome
        {
            get { return nome; }
        }
        public int Idade
        {
            get { return idade; }
            set { idade = value; }
        }

        // public int CompareTo(Pessoa outro)
        // {
        //     if (outro == null) return 1;
            
        //     return idade.CompareTo(outro.idade);
        // }

        public static int CompareByNome(Pessoa p1, Pessoa p2)
        {
            return String.Compare(p1.Nome, p2.Nome);
        }
        public static int ComparebyIdade(Pessoa p1, Pessoa p2)
        {
            return p1.Idade.CompareTo(p2.Idade);
        }
    }
}
